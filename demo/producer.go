package main

import (
	"database/sql"
  "fmt"
  "log"
  //"time"

  "github.com/nsqio/go-nsq"
  _ "gitee.com/opengauss/openGauss-connector-go-pq"
)


func main() {
  config := nsq.NewConfig()
  p, err := nsq.NewProducer("xxx.xxx.xxx.xxx:4150", config)
  if err != nil {
    log.Panic(err)
  }
  
  //连接数据库
  connStr := "user=xxx password=xxxxx dbname=xxx host=xxx.xxx.xxx.xxx port=26000 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := sql.Open("opengauss", connStr)
	if err!= nil {
      panic(err)
    }
  var (
	id string
	//name string
	)
  rows, err := db.Query("select id from index_num")
	if err!= nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id)
		err = p.Publish("testTopic", []byte(id))
		//time.Sleep(time.Second * 1)
    if err != nil {
    	log.Panic(err)
   	} 
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	
  p.Stop()
}
