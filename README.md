# opensource-demo-nsq

nsq适配gaussDB

# 环境安装

下载NSQ安装包并解压

tar zxvf nsq-1.3.0.linux-arm64.go1.21.5.tar.gz ./nsq

进入 nsq/bin目录, 开三个终端,分别按顺序启动
# 启动nsqlookupd
$ ./nsqlookupd
# 启动 nsqd
$ ./nsqd --lookupd-tcp-address=127.0.0.1:4160
# 启动 nsqadmin
$ ./nsqadmin --lookupd-http-address=127.0.0.1:4161
# 启动 消费者和生产者脚本 开启模拟DEMO消费场景
go run consumer.go   
go run producer.go